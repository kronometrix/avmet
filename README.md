# kavmet
Vaisala AviMet to Kronometrix Databus

## Description
The scope of AviMet to Kronometrix databus, is to allow data coming from Vaisala AviMet software to be delivered 
to Kronometrix analytics platform.

## Functions

Vaisala AviMet software is sending data using data messages detailed in Vaisala AviMet Manual and specification document. 
AviMet gate should be able to perform the following core functions:

  * detect valid AviMet traffic, data messages
  
  * convert AviMet data into valid Kronometrix data messages based on LMO AviMet data object defintions
  
  * send it forward to Kronometrix backend, using Kronometrix API
  
  * discard all the other traffic

## Input

  * AviMet data message

## Output

  * Kronometrix data message(s)
