local lmo = require "lmo"
local config = require "conf.config"

local find = string.find
local sub = string.sub
local log = ngx.log
local time = ngx.time
local concat = table.concat

local ERR = ngx.ERR
local WARN = ngx.WARN
local INFO = ngx.INFO

local DEBUG

local _M = {
    _VERSION = '0.1'
}

-- explode(seperator, string)
local function explode(d, p)
    local t, ll
    t={}
    ll=0
    if(#p == 1) then
        return {p}
    end
    while true do
        l = find(p, d, ll, true) -- find the next d in the string
        if l ~= nil then -- if "not not" found then..
            table.insert(t, sub(p, ll, l-1)) -- Save it in our array.
            ll = l + 1 -- save just after where we found it for searching next time.
        else
            table.insert(t, sub(p, ll)) -- Save what's left in our array.
            break -- Break at end, as it should be, according to the lua manual.
        end
    end
    return t
end

function _M.parse_message(message)
    -- process header
    if string.byte(message) ~= 1 then
        return nil, nil, "Wrong SOH character"
    end

    local s = find(message, "\003", 2, true)
    if type(s) == 'number' and s > 2 then
        local header = sub(message, 2, s-1)
        local h_fields = explode(config.avimet.separator, header)

        if #h_fields ~= config.avimet.header.fields then
            return nil, nil, "Wrong header format or wrong version"
        end

        local mid = h_fields[config.avimet.header.mid]
        local tid = h_fields[config.avimet.header.tid]
        local sid = h_fields[config.avimet.header.sid]
        local sensor_id = h_fields[config.avimet.header.sensor_id]
        local location_id = h_fields[config.avimet.header.location_id]
        local icao = h_fields[config.avimet.header.icao]
        if type(tid) ~= 'string' or #tid == 0 
            or type(sid) ~= 'string' or #sid == 0 
            or type(mid) ~= 'string' or #mid == 0 
            or type(sensor_id) ~= 'string' or #sensor_id == 0 
            or type(location_id) ~= 'string' or #location_id == 0
            or type(icao) ~= 'string' or #icao == 0 then
                return nil, nil, "Incomplete header data"
        end

        -- Raw message logging for debug purposes
        if config.avimet.debug and type(config.avimet.debug_path) == 'string' then
            local f_name = 'avimet.' .. icao ..'.log'
            local f, err = io.open(config.avimet.debug_path .. f_name, "a")
            if f then
                f:write(message)
                f:close()
            end
        end

        local ts = time()
        -- if the header has message time, set it as the timestamp
        if tonumber(h_fields[3]) then
            ts = tonumber(h_fields[3])
        end

        -- get all data fields
        local dfe, dfs = s, s
        local d_fields = {}
        while true do
            dfs = find(message, "\002", dfe + 1, true)
            if dfs then
                dfe = find(message, "\003", dfs + 1, true)
                local dfss = find(message, "\002", dfs + 1, true)
                if dfe and dfss and dfss < dfe then
                    dfs = dfss
                elseif not dfe then
                    break
                end
                if dfe - 1 <= dfs + 1 then
                    break
                end
                local dfm = sub(message, dfs + 1, dfe - 1)
                local dfm_a = explode(config.avimet.separator, dfm)

                -- process data field message
                if #dfm_a == 5 then
                    if dfm_a[1] == config.avimet.messages.time then
                        if dfm_a[2] == 'I' and dfm_a[3] == 'N' then
                            ts = tonumber(dfm_a[4])
                        end
                    else
                        if dfm_a[3] == 'N' or dfm_a[3] == 'M' or dfm_a[3] == 'C' or dfm_a[3] == 'O' then
                            if dfm_a[2] == 'I' or dfm_a[2] == 'R' then
                                if tonumber(dfm_a[4]) then
                                    d_fields[dfm_a[1]] = tonumber(dfm_a[4])
                                else
                                    d_fields[dfm_a[1]] = config.avimet.notavailable
                                end
                            else
                                d_fields[dfm_a[1]] = dfm_a[4]
                            end
                        else
                            d_fields[dfm_a[1]] = config.avimet.notavailable
                        end
                    end
                else
                    log(WARN, "Wrong data field length: ", dfm)
                end
            else
                break
            end
        end

        -- generate message based on the data fields and the message type (from header)
        local lmo_def = lmo[config.avimet.messages.lmo[mid]]
        if type(lmo_def) == 'table' and type(lmo_def['format']) == 'table' and type(lmo_def['format']['fields']) == 'table' then
            local m_vals = { lmo_def.id }
            for _, field in ipairs(lmo_def['format']['fields']) do
                if field.role then
                    if field.role == 'subscription_id' then
                        m_vals[#m_vals+1] = sid
                    elseif field.role == 'ds_id' then
                        m_vals[#m_vals+1] = icao -- will be generated automatically
                    elseif field.role == 'device_id' then
                        m_vals[#m_vals+1] = concat({mid, sensor_id, location_id}, "_")
                    end
                elseif field.type and field.type == 'timestamp' then
                    m_vals[#m_vals+1] = ts
                elseif field.name then
                    if d_fields[field.name] then
                        m_vals[#m_vals+1] = d_fields[field.name]
                    else
                        m_vals[#m_vals+1] = config.avimet.notavailable
                    end
                end
            end
            local msg = concat(m_vals, lmo_def['format']['fields_separator'])
            return msg, tid
        else
            return nil, nil, "Message not found in LMO: "..mid
        end
    else
        return nil, nil, "Missing header; message: "..message
    end
end

return _M