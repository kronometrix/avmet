local app = {
    prefix = "/opt/kronometrix/avmet/"
}

local lmo = {
    file = app.prefix .. "app/conf/messages.json"
}

local avimet = {
    separator = "|",
    notavailable = "NA",
    header = {
        fields = 9, -- expected number of header fields
        version = "1", -- expected version
        mid = 4, -- message ID field index
        tid = 8, -- tid field index
        sid = 9, -- sid field index
        sensor_id = 5, -- sensor ID field index
        location_id = 6, -- location ID field index
        icao = 7 -- ICAO code field index
    },
    messages = {
        time = 'TIME',
        lmo = {
            -- AviMet
            ['CLOUD'] = 'amd-avimet-cloud',
            ['HUMITEMP'] = 'amd-avimet-humitemp',
            ['PRESS'] = 'amd-avimet-press',
            ['RAIN'] = 'amd-avimet-rain',
            ['VIS'] = 'amd-avimet-vis',
            ['WIND'] = 'amd-avimet-wind',
            ['RWYFRICTION'] = 'amd-avimet-rwyfriction',
            ['WINDSHEAR'] = 'amd-avimet-windshear',
            ['PW'] = 'amd-avimet-pw',
            ['PV'] = 'amd-avimet-pv',
            ['TSS'] = 'amd-avimet-tss',

            ['METAR'] = 'amd-avimet-metar',
            ['METREP'] = 'amd-avimet-metrep',
            ['SNOWTAM'] = 'amd-avimet-snowtam',

            -- TacMet
            ['TCLOUD'] = 'amd-tacmet-cloud',
            ['THUMITEMP'] = 'amd-tacmet-humitemp',
            ['TPRESS'] = 'amd-tacmet-press',
            ['TRAIN'] = 'amd-tacmet-rain',
            ['TVIS'] = 'amd-tacmet-vis',
            ['TWIND'] = 'amd-tacmet-wind',
            ['TRWYFRICTION'] = 'amd-tacmet-rwyfriction',
            ['TWINDSHEAR'] = 'amd-tacmet-windshear',
            ['TPW'] = 'amd-tacmet-pw',
            ['TPV'] = 'amd-tacmet-pv',
            ['TTSS'] = 'amd-tacmet-tss',

            ['TMETAR'] = 'amd-tacmet-metar',
            ['TMETREP'] = 'amd-tacmet-metrep'
        }
    },
    --debug = true,
    --debug_path = '/usr/home/krmx/avimet_logs/'
}

local kronometrix = {
    {
        host = "127.0.0.1",
        port = 80,
        path = "/api/private/send_data"
    },
    {
        host = "avia.kronometrix.com",
        port = 80,
        path = "/api/private/send_data"
    }
}

return {
    app = app,
    lmo = lmo,
    avimet = avimet,
    kronometrix = kronometrix
}
