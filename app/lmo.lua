local cjson = require "cjson.safe"
local jsondecode = cjson.decode
local config = require "conf.config"
local lmofile = config.lmo.file
local find = string.find
local io_open = io.open
local ngx_time = ngx.time
local log = ngx.log
local ERR = ngx.ERR
local lmo = { messages = {} }

local function readcontent_messages(content)
    if not content then return end
    local obj = jsondecode(content)
    if not obj then
        log(ERR, "Unable to decode LMO messages content")
        return
    end
    lmo.messages[#lmo.messages+1] = { data = obj }
    for dstype, dsdata in pairs(obj.ds) do
        local messages = dsdata.messages
        if type(messages) == "table" then
            for _, message in pairs(messages) do
                local id = message.id
                message.ds_type = dstype
                message.lmo = obj.id
                message.lmo_name = obj.name
                lmo[id] = message
                lmo[#lmo+1] = id

                if not message.stall or not tonumber(message.stall) then
                    log(ERR, "Message ", message.id, " doesn't have the stall value defined")
                end
            end
        end
    end
end

local function readfile(path)
    if not path then return end
    local file = io_open(path, "r")
    local content
    if file then
        content = file:read("*all")
        file:close()
    else
        log(ERR, "Unable to read LMO file ", path)
    end
    return content
end

function lmo.load(ts)
    lmo.timestamp = ts or ngx_time()
    readcontent_messages(readfile(lmofile))
end

function lmo.find(message)
    for _, id in ipairs(lmo) do
        if find(message, id, 1, true) == 1 then
            return id
        end
    end
end

return lmo