local parser = require "parser"
local sender = require "sender"

local log = ngx.log
local concat = table.concat

local ERR = ngx.ERR
local WARN = ngx.WARN
local INFO = ngx.INFO

local sock = ngx.req.socket()
sock:settimeout(3600*1000) -- timeout
while sock do
    local reader = sock:receiveuntil("\004")
    local message, err, partial = reader()
    if message then
        --log(INFO, "BRUT: ", message)
        local msg, tid, err = parser.parse_message(message)
        if err then
            log(WARN, err)
        else
            log(INFO, "Sending message: ", msg)
            local err = sender.send_message(msg, tid)
            if err then
                log(WARN, err)
            end
        end
    else
        break
    end
end